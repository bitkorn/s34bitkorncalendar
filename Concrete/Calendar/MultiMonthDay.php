<?php

namespace Bitkorn\Calendar\Concrete\Calendar;

use Bitkorn\Calendar\Calendar\Day;

/**
 * Description of DayCustom
 *
 * @author allapow
 */
class MultiMonthDay extends Day
{

    /**
     * $viewPositionId indexed Array
     * @var \Bitkorn\Calendar\Attachment\BaseAttachment[]
     */
    private $attachmentsAssoc = [];

    /**
     * $viewPositionId indexed Array
     * @var \Bitkorn\Calendar\Term\PeriodDay[]
     */
    private $periodDaysAssoc = [];

    public function __toString()
    {
        $html = '';
        $classOuter = 'day-header';
        $classOuter .= ($this->date('N') == 6) ? ' saturday': '';
        $classOuter .= ($this->date('N') == 7) ? ' sunday': '';
        $html .= '<div style="width: 100%;" class="'.$classOuter.'">' . $this->date('D - d') . '</div>';
        if (!empty($this->periodDays)) {
            $this->computePeriodDaysAssoc();
        }
        if (!empty($this->attachments)) {
            $this->computeAttachmentsAssoc();
        }
        return $html;
    }

    public function computePeriodDaysAssoc()
    {
        foreach ($this->periodDays as $periodDay) {
            $viewPositionId = $periodDay->getViewPositionId();
            if (array_key_exists($viewPositionId, $this->periodDaysAssoc)) {
                $viewPositionId = $this->computeNewPeriodDayViewPositionId($viewPositionId);
            }
            $this->periodDaysAssoc[$viewPositionId] = $periodDay->setViewPositionId($viewPositionId);
        }
        if (!empty($this->periodDaysAssoc)) {
            ksort($this->periodDaysAssoc);
        }
    }

    private function computeNewPeriodDayViewPositionId($viewPositionId): string
    {
        $viewPositionId .= substr(md5(microtime()), rand(0, 26), 1);
        if (array_key_exists($viewPositionId, $this->periodDaysAssoc)) {
            return computeNewPeriodDayViewPositionId($viewPositionId);
        }
        return $viewPositionId;
    }

    public function computeAttachmentsAssoc()
    {
        foreach ($this->attachments as $attachment) {
            $viewPositionId = $attachment->getViewPositionId();
            if (array_key_exists($viewPositionId, $this->attachmentsAssoc)) {
                $viewPositionId = $this->computeNewAttachmentViewPositionId($viewPositionId);
            }
            $this->attachmentsAssoc[$viewPositionId] = $attachment->setViewPositionId($viewPositionId);
        }
        if (!empty($this->attachmentsAssoc)) {
            ksort($this->attachmentsAssoc);
        }
    }

    private function computeNewAttachmentViewPositionId($viewPositionId): string
    {
        $viewPositionId .= substr(md5(microtime()), rand(0, 26), 1);
        if (array_key_exists($viewPositionId, $this->attachmentsAssoc)) {
            return computeNewAttachmentViewPositionId($viewPositionId);
        }
        return $viewPositionId;
    }

    /**
     * 
     * @param string $viewPositionId
     * @return \Bitkorn\Calendar\Attachment\BaseAttachment
     */
    public function getAttachment($viewPositionId) {
        if(array_key_exists($viewPositionId, $this->periodDaysAssoc)) {
            return $this->periodDaysAssoc[$viewPositionId]->getAttachment();
        }
        if(array_key_exists($viewPositionId, $this->attachmentsAssoc)) {
            return $this->attachmentsAssoc[$viewPositionId];
        }
        return null;
    }
    
}
