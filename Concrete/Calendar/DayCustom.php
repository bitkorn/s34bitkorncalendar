<?php

namespace Bitkorn\Calendar\Concrete\Calendar;

use Bitkorn\Calendar\Calendar\Day;

/**
 * Description of DayCustom
 *
 * @author allapow
 */
class DayCustom extends Day
{

    /**
     * $viewPositionId indexed Array
     * @var \Bitkorn\Calendar\Attachment\BaseAttachment[]
     */
    private $attachmentsAssoc = [];

    /**
     * $viewPositionId indexed Array
     * @var \Bitkorn\Calendar\Term\PeriodDay[]
     */
    private $periodDaysAssoc = [];

    public function __toString()
    {
        $html = '';
        $html .= '<div style="width: 100%;" class="day-header">' . $this->getDayOfMonth() . '</div>';
        if (!empty($this->periodDays)) {
            $this->computePeriodDaysAssoc();
            foreach ($this->periodDaysAssoc as $periodDay) {
                $html .= '<div class="day-periodday">' . $periodDay->getAttachment()->getViewHtml() . '</div>';
            }
        }
        if (!empty($this->attachments)) {
            $this->computeAttachmentsAssoc();
            foreach ($this->attachmentsAssoc as $attachment) {
                $html .= '<div class="day-attachment">' . $attachment->getViewHtml() . '</div>';
            }
        }
        return $html;
    }

    public function computeAttachmentsAssoc()
    {
        foreach ($this->attachments as $attachment) {
            $viewPositionId = $attachment->getViewPositionId();
            if (array_key_exists($viewPositionId, $this->attachmentsAssoc)) {
                $viewPositionId = $this->computeNewAttachmentViewPositionId($viewPositionId);
            }
            $this->attachmentsAssoc[$viewPositionId] = $attachment->setViewPositionId($viewPositionId);
        }
        if (!empty($this->attachmentsAssoc)) {
            ksort($this->attachmentsAssoc);
        }
    }

    private function computeNewAttachmentViewPositionId($viewPositionId): string
    {
        $viewPositionId .= substr(md5(microtime()), rand(0, 26), 1);
        if (array_key_exists($viewPositionId, $this->attachmentsAssoc)) {
            return computeNewAttachmentViewPositionId($viewPositionId);
        }
        return $viewPositionId;
    }

    public function computePeriodDaysAssoc()
    {
        foreach ($this->periodDays as $periodDay) {
            $viewPositionId = $periodDay->getViewPositionId();
            if (array_key_exists($viewPositionId, $this->periodDaysAssoc)) {
                $viewPositionId = $this->computeNewPeriodDayViewPositionId($viewPositionId);
            }
            $this->periodDaysAssoc[$viewPositionId] = $periodDay->setViewPositionId($viewPositionId);
        }
        if (!empty($this->periodDaysAssoc)) {
            ksort($this->periodDaysAssoc);
        }
    }

    private function computeNewPeriodDayViewPositionId($viewPositionId): string
    {
        $viewPositionId .= substr(md5(microtime()), rand(0, 26), 1);
        if (array_key_exists($viewPositionId, $this->periodDaysAssoc)) {
            return computeNewPeriodDayViewPositionId($viewPositionId);
        }
        return $viewPositionId;
    }

}
