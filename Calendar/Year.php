<?php

namespace Bitkorn\Calendar\Calendar;

/**
 *
 * @author allapow
 */
class Year implements \Iterator, \ArrayAccess
{
    /**
     *
     * @var int
     */
    protected $yearNum;
    
    /**
     *
     * @var Month[]
     */
    protected $yearGrid = [];

    /**
     *
     * @var int
     */
    private $iterateIndex = 1;
    
    /**
     * 
     * @param int $yearNum
     */
    public function __construct(int $yearNum)
    {
        $this->yearNum = $yearNum;
        $this->computeYearGrid();
    }
    
    private function computeYearGrid()
    {
        for($i = 1; $i <= 12; $i++) {
            $this->yearGrid[$i] = new Month($i, $this->yearNum);
        }
    }

    /**
     * 
     * @return \Bitkorn\Calendar\Month
     */
    public function current(): Month
    {
        return $this->yearGrid[$this->iterateIndex];
    }

    /**
     * 
     * @return int
     */
    public function key(): int
    {
        return $this->iterateIndex;
    }

    public function next()
    {
        ++$this->iterateIndex;
    }

    /**
     * 
     * @param int $offset
     * @return bool
     */
    public function offsetExists(int $offset): bool
    {
        return isset($this->yearGrid[$offset]);
    }

    /**
     * 
     * @param int $offset
     * @return \Bitkorn\Calendar\Month
     */
    public function offsetGet(int $offset): Month
    {
        return $this->yearGrid[$offset];
    }

    /**
     * 
     * @param int $offset
     * @param \Bitkorn\Calendar\Month $value
     */
    public function offsetSet(int $offset, Month $value)
    {
        $this->yearGrid[$offset] = $value;
    }

    /**
     * 
     * @param int $offset
     */
    public function offsetUnset(int $offset)
    {
        unset($this->yearGrid[$offset]);
    }

    public function rewind()
    {
        $this->iterateIndex = 1;
    }

    /**
     * 
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->yearGrid[$this->iterateIndex]);
    }
    
    /**
     * 
     * @return Year
     */
    public function getYearNum(): Year
    {
        return $this->yearNum;
    }


}
