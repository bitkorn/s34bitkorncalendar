<?php

namespace Bitkorn\Calendar\Calendar;

use Bitkorn\Calendar\Attachment\BaseAttachment;
use Bitkorn\Calendar\Term\PeriodDay;

/**
 *
 * @author allapow
 */
class Month implements \Iterator, \ArrayAccess, \Countable
{

    /**
     *
     * @var \Zend\Log\Logger
     */
    protected $logger;

    /**
     *
     * @var int
     */
    protected $monthNum;

    /**
     *
     * @var int
     */
    protected $yearNum;

    /**
     *
     * @var int Computed in constructor through $month and $year.
     */
    protected $dayCount;

    /**
     *
     * @var int The empty calendar fields before the first day of month.
     */
    protected $prependCount;

    /**
     *
     * @var int The empty calendar fields after the last day of month.
     */
    protected $appendCount;

    /**
     *
     * @var int Count days in month plus $prependCount and $appendCount empty days for Mo-So-Grid.
     */
    protected $gridCount;

    /**
     * UNUSED
     * @var int Um die Woche im CalendarGrid mit verschiedenen Tagen starten zu koennen.
     */
    private $firstWeekday = 1; // 0=Sonntag; 1=Montag

    /**
     *
     * @var Day[] Key: the grid index (1 - $this->gridCount); value: a Day object.
     */
    protected $monthGrid = [];

    /**
     *
     * @var int
     */
    private $iterateIndex = 1;

    /**
     *
     * @var string Day class to reference
     */
    protected $customDayClass;

    /**
     * Assoc Array with key=ISO8601 and value=[]
     * @var [ISO8601] => \Bitkorn\Calendar\Attachment\BaseAttachment[]
     */
    protected $attachments = [];

    /**
     * Assoc Array with key=ISO8601 and value=[]
     * @var [ISO8601] => \Bitkorn\Calendar\Term\PeriodDay[]
     */
    protected $periodDays = [];

    /**
     * 
     * @param int $monthNum
     * @param int $yearNum
     */
    public function __construct(int $monthNum, int $yearNum)
    {
        $this->monthNum = (int) $monthNum;
        $this->yearNum = (int) $yearNum;
        $this->dayCount = cal_days_in_month(CAL_GREGORIAN, $this->monthNum, $this->yearNum);
    }

    /**
     * 
     * @param \Zend\Log\Logger $logger
     */
    public function setLogger(\Zend\Log\Logger $logger)
    {
        $this->logger = $logger;
    }

    public function setCustomDayClass(string $customDayClass)
    {
//        $customDayClassObj = new $customDayClass;
        if (class_exists($customDayClass)) {
            $this->customDayClass = $customDayClass;
        }
    }

    public function addAttachment(string $iso8601, BaseAttachment $attachment)
    {
        if (array_key_exists($iso8601, $this->attachments)) {
            $this->attachments[$iso8601][] = $attachment;
        } else {
            $this->attachments[$iso8601] = [$attachment];
        }
    }

    /**
     * Disassembles a PeriodDay
     * 
     * @param PeriodDay $periodDay
     */
    public function addPeriodDay(PeriodDay $periodDay)
    {
        foreach ($periodDay as $iso8601) {
            if (!array_key_exists($iso8601, $this->periodDays)) {
                $this->periodDays[$iso8601] = [];
            }
            $this->periodDays[$iso8601][] = $periodDay;
        }
    }

    /**
     * 
     * @return array
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * 
     * @return array
     */
    public function getPeriodDays(): array
    {
        return $this->periodDays;
    }

    /**
     * 
     */
    public function computeMonthGrid(bool $attach = true)
    {
        $this->prependCount = date('N', strtotime($this->yearNum . '-' . str_pad($this->monthNum, 2, '0', STR_PAD_LEFT) . '-1')) - 1;
        /*
         * Ends $this->dayCount + $this->prependCount on last weekday, then seven empty days are appended ($this->appendCount = 7).
         * e.g. 2016-07
         */
        $this->appendCount = 7 - (($this->dayCount + $this->prependCount) % 7);
        $this->gridCount = $this->prependCount + $this->dayCount + $this->appendCount;

        $dayOfMonth = 1;
        for ($i = 1; $i <= $this->gridCount; $i++) {
            if ($i < $this->prependCount) {
                // empty prepend days
                $this->monthGrid[$i] = ''; // muss isset (kein null), sonst kein iterate wegen valid()
            } elseif ($i > $this->prependCount && $i <= ($this->prependCount + $this->dayCount)) {
                // days of month
                $iso8601 = $this->yearNum . '-'
                        . str_pad($this->monthNum, 2, '0', STR_PAD_LEFT) . '-'
                        . str_pad($dayOfMonth, 2, '0', STR_PAD_LEFT);
                if (empty($this->customDayClass)) {
                    $day = new Day($i, str_pad($dayOfMonth, 2, '0', STR_PAD_LEFT), $iso8601);
                } elseif (class_exists($this->customDayClass)) {
                    $day = new $this->customDayClass($i, str_pad($dayOfMonth, 2, '0', STR_PAD_LEFT), $iso8601);
                } else {
                    throw new \Exception('Class ' . $this->customDayClass . ' does not exist.');
                }
                if ($attach) {
                    if (!empty($this->attachments) && is_array($this->attachments) && array_key_exists($iso8601, $this->attachments) && is_array($this->attachments[$iso8601]) && !empty($this->attachments[$iso8601])) {
                        $day->setAttachments($this->attachments[$iso8601]);
                    }
                    if (!empty($this->periodDays) && is_array($this->periodDays) && array_key_exists($iso8601, $this->periodDays) && is_array($this->periodDays[$iso8601]) && !empty($this->periodDays[$iso8601])) {
                        $day->setPeriodDays($this->periodDays[$iso8601]);
                    }
                }

                $this->monthGrid[$i] = $day;
                $dayOfMonth++;
            } else {
                // empty append days
                $this->monthGrid[$i] = ''; // muss isset (kein null), sonst kein iterate wegen valid()
            }
        }
    }

    public function __toString()
    {
        $html = '';
//        $html .= 'dayCount: ' . $this->dayCount . '<br>';
//        $html .= 'gridCount: ' . $this->gridCount . '<br>';
//        $html .= 'prependCount: ' . $this->prependCount . '<br>';
//        $html .= 'appendCount: ' . $this->appendCount . '<br>';
//        $html .= '<textarea>' . print_r($this->monthGrid, true) . '</textarea><br>';
//        $html .= '<textarea>' . print_r($this, true) . '</textarea><br>';
        $html .= '<div style="width: 100%;">';
        foreach ($this as $day) {
            if ($day instanceof Day) {
                $html .= '<div style="width: 14%; border: 1px solid #ccc; float: left;">' . $day . '</div>';
            } else {
                $html .= '<div style="width: 14%; border: 1px solid #ccc; float: left;">&nbsp;</div>';
            }
        }
        $html .= '</div>';
        return $html;
    }

    /**
     * 
     * @return int Day in this month computed in constructor
     */
    public function getDayCount(): int
    {
        return $this->dayCount;
    }

    /**
     * 
     * @return Day|string
     */
    public function current()
    {
//        var_dump(__METHOD__);
        return $this->monthGrid[$this->iterateIndex];
    }

    public function key()
    {
//        var_dump(__METHOD__);
        return $this->iterateIndex;
    }

    public function next()
    {
//        var_dump(__METHOD__);
        ++$this->iterateIndex;
    }

    public function rewind()
    {
//        var_dump(__METHOD__);
        $this->iterateIndex = 1;
    }

    public function valid()
    {
//        var_dump(__METHOD__);
        return isset($this->monthGrid[$this->iterateIndex]);
    }

    public function offsetExists($offset)
    {
        return isset($this->monthGrid[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->monthGrid[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->monthGrid[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->monthGrid[$offset]);
    }

    public function count()
    {
        return count($this->monthGrid);
    }

    /**
     * 
     * @return Day
     */
    public function firstDay()
    {
        return $this->monthGrid[$this->prependCount + 1];
    }

    /**
     * 
     * @return Day
     */
    public function lastDay()
    {
        return $this->monthGrid[$this->count() - $this->appendCount];
    }

    /**
     * 
     * @return int
     */
    public function getCurrentWeeknumber()
    {
        if ($this->iterateIndex < 8) {
            return date('W', strtotime($this->yearNum . '-' . $this->monthNum . '-' . $this->offsetGet($this->prependCount + 1)->getDayOfMonth()));
        } elseif ($this->iterateIndex > ($this->prependCount + $this->gridCount)) {
            return date('W',
                    strtotime($this->yearNum . '-' . $this->monthNum . '-' . $this->offsetGet($this->prependCount + $this->gridCount - 1)->getDayOfMonth()));
        } else {
            $currentDay = $this->current();
            /**
             * abfragen weil die letzte Reihe auch ganz ohne Tage sein kann
             */
            if (isset($currentDay) && $currentDay instanceof Day) {
                return date('W', strtotime($this->yearNum . '-' . $this->monthNum . '-' . $currentDay->getDayOfMonth()));
            } else {
                return '';
            }
        }
    }

    public function dateYearMonth(string $format)
    {
        return date($format, strtotime($this->yearNum . '-' . $this->monthNum . '-01'));
    }

}
