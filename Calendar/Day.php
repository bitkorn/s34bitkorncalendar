<?php

namespace Bitkorn\Calendar\Calendar;

use Bitkorn\Calendar\Attachment\BaseAttachment;
use Bitkorn\Calendar\Term\PeriodDay;

/**
 *
 * @author allapow
 */
class Day
{

    /**
     *
     * @var int
     */
    protected $gridIndex;

    /**
     *
     * @var int
     */
    protected $dayOfMonth;

    /**
     *
     * @var string
     */
    protected $iso8601;

    /**
     * 0 indexed Array
     * @var \Bitkorn\Calendar\Attachment\BaseAttachment[]
     */
    protected $attachments = [];

    /**
     * 0 indexed Array
     * @var \Bitkorn\Calendar\Term\PeriodDay[]
     */
    protected $periodDays = [];

    /**
     * 
     * @param int $gridIndex The grid index.
     * @param int $dayOfMonth The day of the month.
     * @param string $iso8601 String representation of the day in ISO 8601 format.
     */
    public function __construct($gridIndex, $dayOfMonth, $iso8601)
    {
        $this->gridIndex = $gridIndex;
        $this->dayOfMonth = $dayOfMonth;
        $this->iso8601 = $iso8601;
    }

    function setAttachments(array $attachments)
    {
        $this->attachments = $attachments;
    }

    function addAttachment(BaseAttachment $attachment)
    {
        $this->attachments[] = $attachment;
    }

//    function addAttachments(array $attachments)
//    {
////        $this->attachments = array_merge($this->attachments, $attachments);
//        foreach ($attachments as $attachment) {
//            $this->attachments[] = $attachment;
//        }
//    }

    function setPeriodDays(array $periodDays)
    {
        $this->periodDays = $periodDays;
    }

    function addPeriodDay(PeriodDay $periodDay)
    {
        $this->periodDays[] = $periodDay;
    }

//    function addPeriodDays(array $periodDays)
//    {
////        $this->periodDays = array_merge($this->periodDays, $periodDays);
//        foreach ($periodDays as $periodDay) {
//            $this->periodDays[] = $periodDay;
//        }
//    }

    public function __toString()
    {
        return 'gridIndex: ' . $this->gridIndex . '; ISO 8601: ' . $this->iso8601 . '; ';
    }

    public function getGridIndex()
    {
        return $this->gridIndex;
    }

    public function getDayOfMonth()
    {
        return $this->dayOfMonth;
    }

    public function getIso8601()
    {
        return $this->iso8601;
    }

    public function getUnixtime()
    {
        return strtotime($this->iso8601);
    }

    public function date(string $format): string
    {
        return date($format, $this->getUnixtime());
    }

}
