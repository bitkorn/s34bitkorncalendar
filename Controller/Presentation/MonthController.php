<?php

namespace Bitkorn\Calendar\Controller\Presentation;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bitkorn\Calendar\Calendar\Month;
use Bitkorn\Calendar\Calendar\Day;
use Bitkorn\Calendar\Concrete\Calendar\DayCustom;
use Bitkorn\Calendar\Attachment\BaseAttachment;
use Bitkorn\Calendar\Term\PeriodMonth;
use Bitkorn\Calendar\Term\PeriodDay;
use Bitkorn\Calendar\Render\Month\MonthTableRenderer;
use Bitkorn\Calendar\Render\Month\MultiMonthTableRenderer;
use Bitkorn\Calendar\Render\Month\MultiMonthHorizontalRenderer;
use Symfony\Component\HttpFoundation\Request;
use Bitkorn\Calendar\Render\MonthRendererAbstract;
use Bitkorn\Calendar\Concrete\Calendar\MultiMonthDay;

/**
 *
 * @author Torsten Brieskorn
 */
class MonthController extends Controller
{

    /**
     * @Route("/presentation/month", name="bitkorn_calendar_presentation_month")
     */
    public function monthPresentationAction()
    {
        $month = new Month(8, 2018);
        $month->setCustomDayClass(DayCustom::class);

        $periodDay = new PeriodDay('2018-07-12', '2018-08-04');
        $attachmentPd01 = new BaseAttachment('a1');
        $attachmentPd01->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">Wanderwochen 01</div>';
        });
        $periodDay->setAttachment($attachmentPd01);
        $periodDay->init();
        $month->addPeriodDay($periodDay);

        $periodDay02 = new PeriodDay('2018-08-12', '2018-09-04');
        $attachmentPd02 = new BaseAttachment('a2');
        $attachmentPd02->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">Wanderwochen 02</div>';
        });
        $periodDay02->setAttachment($attachmentPd02);
        $periodDay02->init();
        $month->addPeriodDay($periodDay02);

        $attachment01 = new BaseAttachment('c4');
        $someZeug = 'zeug';
//        $replaceFunc = function($viewHtml){ return 'my own HTML';};
        $attachment01->replaceViewHtml(function($viewHtml) use ($someZeug) {
            return 'with ' . $someZeug;
        });
        $month->addAttachment('2018-08-03', $attachment01);

        $month->computeMonthGrid();

        $monthRenderer = new MonthTableRenderer($month);

        return $this->render('@QondanaCalendar/presentation/month.html.twig', ['monthRenderer' => $monthRenderer]);
    }

    /**
     * @Route("/presentation/multi-month", name="bitkorn_calendar_presentation_multimonth")
     */
    public function multiMonthPresentationAction(Request $request)
    {
        $startIso8601 = $request->get('start', '2018-07-12');
        $endIso8601 = $request->get('end', '2018-09-22');

        $periodDay = new PeriodDay('2018-07-08', '2018-08-04');
        $attachmentPd01 = new BaseAttachment('a1');
        $attachmentPd01->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">Wanderwochen 01</div>';
        });
        $periodDay->setAttachment($attachmentPd01);
        $periodDay->init();

        $periodDay02 = new PeriodDay('2018-08-12', '2018-09-04');
        $attachmentPd02 = new BaseAttachment('a2');
        $attachmentPd02->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">Wanderwochen 02</div>';
        });
        $periodDay02->setAttachment($attachmentPd02);
        $periodDay02->init();


        $periodMonth = new PeriodMonth($startIso8601, $endIso8601);
        $periodMonth->init();

        $multiMonthRenderer = new MultiMonthTableRenderer($periodMonth);
        $multiMonthRenderer->addPeriodDay($periodDay);
        $multiMonthRenderer->addPeriodDay($periodDay02);
        $multiMonthRenderer->init();

        /*
         * oder so
         */
//        $multiMonthRenderer = new \Bitkorn\Calendar\Render\MultiMonthRendererAbstract($periodMonth);
//        $multiMonthRenderer->setMonthRendererClass(MonthTableRenderer::class);
//        $multiMonthRenderer->setDayClass(DayCustom::class);
//        $multiMonthRenderer->addPeriodDay($periodDay);
//        $multiMonthRenderer->addPeriodDay($periodDay02);
//        $multiMonthRenderer->init();

        return $this->render('@QondanaCalendar/presentation/multi-month.html.twig', ['multiMonthRenderer' => $multiMonthRenderer]);
    }

    /**
     * @Route("/presentation/multi-month-horizontal", name="bitkorn_calendar_presentation_multimonthhorizontal")
     */
    public function multiMonthHorizontalPresentationAction(Request $request)
    {
        $startIso8601 = $request->get('start', '2018-07-12');
        $endIso8601 = $request->get('end', '2018-09-22');

        $periodDay = new PeriodDay('2018-07-08', '2018-08-04');
        $attachmentPd01 = new BaseAttachment('a1');
        $attachmentPd01->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">WW 01</div>';
        });
        $periodDay->setAttachment($attachmentPd01);
        $periodDay->init();

        $periodDay02 = new PeriodDay('2018-08-12', '2018-09-04');
        $attachmentPd02 = new BaseAttachment('a1');
        $attachmentPd02->replaceViewHtml(function($viewHtml) {
            return '<div class="term-header">WW 02</div>';
        });
        $periodDay02->setAttachment($attachmentPd02);
        $periodDay02->init();


        $periodDayUser01 = new PeriodDay('2018-07-12', '2018-08-02');
        $attachmentUser01 = new BaseAttachment('ua');
        $attachmentUser01->replaceViewHtml(function($viewHtml) {
            return 'torti';
        });
        $periodDayUser01->setAttachment($attachmentUser01);
        $periodDayUser01->init();

        $periodDayUser02 = new PeriodDay('2018-07-08', '2018-07-27');
        $attachmentUser02 = new BaseAttachment('ub');
        $attachmentUser02->replaceViewHtml(function($viewHtml) {
            return 'Alfred';
        });
        $periodDayUser02->setAttachment($attachmentUser02);
        $periodDayUser02->init();

        $periodDayUser03 = new PeriodDay('2018-07-16', '2018-07-29');
        $attachmentUser03 = new BaseAttachment('uc');
        $attachmentUser03->replaceViewHtml(function($viewHtml) {
            return 'Holger';
        });
        $periodDayUser03->setAttachment($attachmentUser03);
        $periodDayUser03->init();

        $periodMonth = new PeriodMonth($startIso8601, $endIso8601);
        $periodMonth->init();

        $multiMonthRenderer = new MultiMonthHorizontalRenderer($periodMonth);
        $multiMonthRenderer->setMonthRendererClass(MonthRendererAbstract::class);
        $multiMonthRenderer->setDayClass(MultiMonthDay::class);
        $multiMonthRenderer->addPeriodDay($periodDay);
        $multiMonthRenderer->addPeriodDay($periodDay02);
        $multiMonthRenderer->addPeriodDay($periodDayUser01);
        $multiMonthRenderer->addPeriodDay($periodDayUser02);
        $multiMonthRenderer->addPeriodDay($periodDayUser03);
        $multiMonthRenderer->init();


        return $this->render('@QondanaCalendar/presentation/multi-month-horizontal.html.twig', ['multiMonthRenderer' => $multiMonthRenderer]);
    }

}
