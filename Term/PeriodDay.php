<?php

namespace Bitkorn\Calendar\Term;

use Bitkorn\Calendar\Attachment\BaseAttachment;

/**
 * PeriodDay ist zuerst eine Periode um seine $iso8601s auf Tage mappen zu können.
 * Dann ist PeriodDay ein Attachment weil nur sein Attachment an einem seiner $iso8601s wichtig ist.
 * 
 * @author Torsten Brieskorn
 */
class PeriodDay implements \ArrayAccess, \Countable, \Iterator
{

    /**
     *
     * @var string
     */
    private $startIso8601;

    /**
     *
     * @var string
     */
    private $endIso8601;

    /**
     *
     * @var array All ISOs from $startIso8601 to $endIso8601
     * 0 indexed array with value=ISO8601
     */
    private $iso8601s;
    
    /**
     *
     * @var array 0 indexed with value=substr($iso8601, 0, 7)
     */
    private $yearMonths = [];

    /**
     *
     * @var string Position ID
     */
    private $viewPositionId = '';

    /**
     * Allways the same for every day.
     * @var \Bitkorn\Calendar\Attachment\BaseAttachment
     */
    private $attachment;

    public function __construct(string $startIso8601, string $endIso8601)
    {
        $this->setStartIso8601($startIso8601);
        $this->setEndIso8601($endIso8601);
    }

    function getStartIso8601()
    {
        return $this->startIso8601;
    }

    function getEndIso8601()
    {
        return $this->endIso8601;
    }

    function setStartIso8601($startIso8601)
    {
        if(!strtotime($startIso8601)) {
            throw new \RuntimeException(__FUNCTION__ . ' Cannot parse date' . $startIso8601);
        }
        $this->startIso8601 = $startIso8601;
    }

    function setEndIso8601($endIso8601)
    {
        if(!strtotime($endIso8601)) {
            throw new \RuntimeException(__FUNCTION__ . ' Cannot parse date' . $endIso8601);
        }
        $this->endIso8601 = $endIso8601;
    }
    
    public function getYearMonths(): array
    {
        return $this->yearMonths;
    }

    function getViewPositionId(): string
    {
        return $this->viewPositionId;
    }

    function setViewPositionId(string $viewPositionId): PeriodDay
    {
        $this->viewPositionId = $viewPositionId;
        return $this;
    }

    function getAttachment(): BaseAttachment
    {
        return $this->attachment;
    }

    function setAttachment(BaseAttachment $attachment)
    {
        $this->attachment = $attachment;
        $this->viewPositionId = $this->attachment->getViewPositionId();
    }

    public function init()
    {
        $begin = new \DateTime($this->startIso8601);
        $end = new \DateTime($this->endIso8601);
        $end = $end->modify( '+1 day' ); // include the end date (http://php.net/manual/de/class.dateperiod.php)

        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);

        foreach ($period as $dt) {
            $iso8601 = $dt->format('Y-m-d');
            $this->iso8601s[$iso8601] = $iso8601;
            if(!in_array(substr($iso8601, 0, 7), $this->yearMonths)) {
                $this->yearMonths[] = substr($iso8601, 0, 7);
            }
        }
    }

    public function __toString()
    {
        return $this->attachment->getViewHtml();
    }

    function getIso8601s()
    {
        return $this->iso8601s;
    }

    public function offsetExists($iso8601): bool
    {
        return isset($this->iso8601s[$iso8601]);
    }

    public function offsetGet($iso8601)
    {
        return $this->iso8601s[$iso8601];
    }

    public function offsetSet($iso8601, $value): void
    {
        $this->iso8601s[$iso8601] = $value;
    }

    public function offsetUnset($iso8601): void
    {
        unset($this->iso8601s[$iso8601]);
    }

    public function count(): int
    {
        return count($this->iso8601s);
    }

    public function current()
    {
        return current($this->iso8601s);
    }

    public function key(): \scalar
    {
        return key($this->iso8601s);
    }

    public function next(): void
    {
        next($this->iso8601s);
    }

    public function rewind(): void
    {
        reset($this->iso8601s);
    }

    public function valid(): bool
    {
        return $this->current() !== false;
    }

}
