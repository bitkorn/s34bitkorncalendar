<?php

namespace Bitkorn\Calendar\Render;

/**
 *
 * @author Torsten Brieskorn
 */
interface RendererInterface
{

    public function getHtml(): string;
}
