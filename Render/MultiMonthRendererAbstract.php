<?php

namespace Bitkorn\Calendar\Render;

use Bitkorn\Calendar\Calendar\Month;
use Bitkorn\Calendar\Term\PeriodMonth;
use Bitkorn\Calendar\Attachment\BaseAttachment;
use Bitkorn\Calendar\Term\PeriodDay;
use Bitkorn\Calendar\Render\MonthRendererAbstract;
use Bitkorn\Calendar\Calendar\Day;

/**
 * 
 *
 * @author Torsten Brieskorn
 */
class MultiMonthRendererAbstract implements RendererInterface
{

    /**
     *
     * @var PeriodMonth
     */
    protected $periodMonth;
    
    /**
     *
     * @var string Class name for new created days.
     */
    protected $dayClass = '';
    
    /**
     *
     * @var string Class name for $this->monthRenderers[]
     */
    protected $monthRendererClass = '';

    /**
     *
     * @var \Bitkorn\Calendar\Render\MonthRendererAbstract[]
     */
    protected $monthRenderers = [];

    /**
     * Assoc Array with key=ISO8601 and value=\Bitkorn\Calendar\Attachment\BaseAttachment[]
     * @var array
     */
    protected $attachments = [];

    /**
     * Assoc Array with key=ISO8601 and value=\Bitkorn\Calendar\Term\PeriodDay[]
     * @var array
     */
    protected $periodDays = [];
    
    /**
     *
     * @var array Assoc with key=$viewPositionId and value=$viewPositionId
     */
    protected $viewPositionIdsAll = [];

    /**
     * 
     * @param PeriodMonth $periodMonth
     */
    public function __construct(PeriodMonth $periodMonth)
    {
        $this->periodMonth = $periodMonth;
    }

    /**
     * 
     * @param string $dayClass
     * @throws \Exception
     */
    public function setDayClass(string $dayClass)
    {
//        if(!class_exists($dayClass) || !new $dayClass() instanceof Day) {
        if(!class_exists($dayClass)) {
            throw new \Exception('Class ' . $dayClass . ' not suitable in ' . __CLASS__ .  '->' . __FUNCTION__ . '() on line ' . __LINE__);
        }
        $this->dayClass = $dayClass;
    }
    
    /**
     * 
     * @param string $monthRendererClass
     * @throws \Exception
     */
    public function setMonthRendererClass(string $monthRendererClass)
    {
//        if(!class_exists($monthRendererClass) || !new $monthRendererClass() instanceof MonthRendererAbstract) {
        if(!class_exists($monthRendererClass)) {
            throw new \Exception('Class ' . $monthRendererClass . ' not suitable in ' . __CLASS__ .  '->' . __FUNCTION__ . '() on line ' . __LINE__);
        }
        $this->monthRendererClass = $monthRendererClass;
    }

    /**
     * 
     * @param string $iso8601
     * @param BaseAttachment $attachment
     */
    public function addAttachment(string $iso8601, BaseAttachment $attachment)
    {
        if (array_key_exists($iso8601, $this->attachments)) {
            $this->attachments[$iso8601][] = $attachment;
        } else {
            $this->attachments[$iso8601] = [$attachment];
        }
        $this->viewPositionIdsAll[$attachment->getViewPositionId()] = $attachment->getViewPositionId();
    }

    /**
     * Disassembles a PeriodDay
     * 
     * @param PeriodDay $periodDay
     */
    public function addPeriodDay(PeriodDay $periodDay)
    {
        foreach ($periodDay->getYearMonths() as $iso8601yearMonth) {
//            $yearMonth = substr($iso8601, 0, 7);
            if (!array_key_exists($iso8601yearMonth, $this->periodDays)) {
                $this->periodDays[$iso8601yearMonth] = [];
            }
            $this->periodDays[$iso8601yearMonth][] = $periodDay;
        }
        $this->viewPositionIdsAll[$periodDay->getViewPositionId()] = $periodDay->getViewPositionId();
    }

    public function init()
    {
        foreach ($this->periodMonth as $iso8601) {
            $month = new Month(substr($iso8601, 5, 2), substr($iso8601, 0, 4));
            $month->setCustomDayClass($this->dayClass);
            foreach ($this->attachments as $iso8601attachment => $attachment) {
                $month->addAttachment($iso8601attachment, $attachment);
            }
            $iso8601yearMonth = substr($iso8601, 0, 7);
            if (array_key_exists($iso8601yearMonth, $this->periodDays)) {
                foreach ($this->periodDays[$iso8601yearMonth] as $periodDay) {
                    $month->addPeriodDay($periodDay);
                }
            }

            $month->computeMonthGrid();
            $this->monthRenderers[] = new $this->monthRendererClass($month);
        }
    }

    public function __toString()
    {
        return $this->getHtml();
    }

    public function getHtml(): string
    {
        $html = '';
        foreach ($this->monthRenderers as $monthRenderer) {
            $html .= '<div class="multi-month">';
            $html .= $monthRenderer->getHtml();
            $html .= '</div>';
        }
        return $html;
    }
    
    public function getAttachmentsAll()
    {
        return $this->viewPositionIdsAll;
    }

}
