<?php

namespace Bitkorn\Calendar\Render\Month;

use Bitkorn\Calendar\Calendar\Month;
use Bitkorn\Calendar\Calendar\Day;
use Bitkorn\Calendar\Render\MonthRendererAbstract;

/**
 *
 * @author Torsten Brieskorn
 */
class MonthTableRenderer extends MonthRendererAbstract
{

    public function __toString()
    {
        return $this->getHtml();
    }

    public function getHtml(): string
    {
        $html = '<table class="month-renderer">';
        $html .= '<tr>'
                . '<th>&nbsp;</th><th class="month-renderer-h" colspan="7">' . $this->month->dateYearMonth('Y M') . '</th>'
                . '</tr>';
        $html .= '<tr>'
                . '<th class="month-renderer-weekno">&nbsp;</th>'
                . '<th>MO</th><th>DI</th><th>MI</th><th>DO</th><th>FR</th><th>SA</th><th>SO</th>'
                . '</tr>';
        foreach ($this->month as $day) {
            $key = $this->month->key();
            if ($key == 1 || ($key % 7) == 1) {
                $html .= '<tr><th class="month-renderer-weekno">' . $this->month->getCurrentWeeknumber() . '</th>';
            }
            if ($day instanceof Day) {
                $html .= '<td class="month-renderer-day">';
                $html .= $day;
            } else {
                $html .= '<td>';
            }
            $html .= '</td>';
            if ($key == 7 || ($key % 7) == 0) {
                $html .= '</tr>';
            }
        }
        $html .= '<table>';
        return $html;
    }

}
