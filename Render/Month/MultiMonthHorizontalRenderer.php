<?php

namespace Bitkorn\Calendar\Render\Month;

use Bitkorn\Calendar\Calendar\Day;
use Bitkorn\Calendar\Calendar\Month;
use Bitkorn\Calendar\Render\Month\MonthTableRenderer;
use Bitkorn\Calendar\Render\MultiMonthRendererAbstract;
use Bitkorn\Calendar\Concrete\Calendar\MultiMonthDay;

/**
 * 
 * 
 * @author Torsten Brieskorn
 */
class MultiMonthHorizontalRenderer extends MultiMonthRendererAbstract
{

    public function getHtml(): string
    {
        $html = '';

        foreach ($this->monthRenderers as $monthTableRenderer) {
            /* @var $month Month */
            $month = $monthTableRenderer->getMonth();
            $html .= '<table class="month-renderer">';
            $html .= '<tr>'
                    . '<th class="month-renderer-h" colspan="' . $month->getDayCount() . '">'
                    . $month->dateYearMonth('Y M')
                    . '</th>'
                    . '</tr>';
            $trs = [];
            foreach ($month as $index => $day) {
                if ($day instanceof MultiMonthDay) {
                    if ($day->getDayOfMonth() == 1) {
                        $trs['day'] = '<tr id="month-renderer-tr-day"><td>' . $day . '</td>';
                    } elseif (isset($month[$index + 1])) {
                        $trs['day'] .= '<td>' . $day . '</td>';
                    } else {
                        $trs['day'] .= '<td>' . $day . '</td></tr>';
                    }
                    foreach ($this->viewPositionIdsAll as $viewPositionId) {
                        
                        if (($attach = $day->getAttachment($viewPositionId))) {
                            $htmlAttach = '<div class="day-periodday">' . $attach->getViewHtml() . '</div>';
                        } else {
                            $htmlAttach = '<div class="day-periodempty">&nbsp;</div>';
                        }

                        if ($day->getDayOfMonth() == 1) {
                            $trs[$viewPositionId] = '<tr id="month-renderer-tr-' . $viewPositionId . '"><td>' . $htmlAttach . '</td>';
                        } elseif (isset($month[$index + 1])) {
                            $trs[$viewPositionId] .= '<td>' . $htmlAttach . '</td>';
                        } else {
                            $trs[$viewPositionId] .= '<td>' . $htmlAttach . '</td></tr>';
                        }
                    }
                }
            }
            foreach($trs as $tr) {
                $html .= $tr;
            }
            $html .= '</table>';
        }
        return $html;
    }

//    public function getHtml(): string
//    {
//        $html = '';
//
//        foreach ($this->monthRenderers as $monthTableRenderer) {
//            /* @var $month Month */
//            $month = $monthTableRenderer->getMonth();
//            $html .= '<table class="month-renderer">';
//            $html .= '<tr>'
//                    . '<th class="month-renderer-h" colspan="' . $month->getDayCount() . '">'
//                    . $month->dateYearMonth('Y M')
//                    . '</th>'
//                    . '</tr>';
//            $html .= '<tr>';
//            $html .= '<tr>';
//            foreach ($month as $day) {
//                if ($day instanceof MultiMonthDay) {
//                    $html .= '<td class="month-renderer-day">';
//                    $html .= $day;
//                    foreach ($this->viewPositionIdsAll as $viewPositionId) {
//                        if(($attach = $day->getAttachment($viewPositionId))) {
//                            $html .= '<div class="day-periodday">' . $attach->getViewHtml() . '</div>';
//                        } else {
//                            $html .= '<div class="day-periodempty">&nbsp;</div>';
//                        }
//                    }
//                    $html .= '</td>';
//                }
//            }
//            $html .= '</tr>';
//            $html .= '</table>';
//        }
//        return $html;
//    }
}
