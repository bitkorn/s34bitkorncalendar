<?php

namespace Bitkorn\Calendar\Render;

use Bitkorn\Calendar\Calendar\Month;

/**
 *
 * @author Torsten Brieskorn
 */
class MonthRendererAbstract implements RendererInterface
{

    /**
     *
     * @var \Bitkorn\Calendar\Calendar\Month
     */
    protected $month;

    public function __construct(Month $month)
    {
        $this->month = $month;
    }

    public function getHtml(): string
    {
        return strval($this->month);
    }

    /**
     * 
     * @return Month
     */
    public function getMonth(): Month
    {
        return $this->month;
    }

}
