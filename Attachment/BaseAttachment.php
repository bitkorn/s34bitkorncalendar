<?php

namespace Bitkorn\Calendar\Attachment;

/**
 * Base class for all attachments.
 *
 * @author allapow
 */
class BaseAttachment {

    /**
     *
     * @var string Position ID
     */
    private $viewPositionId;

    /**
     *
     * @var string
     */
    private $viewHtml = 'baHtml';

    function __construct(string $viewPositionId) {
        $this->viewPositionId = $viewPositionId;
    }

    function getViewPositionId(): string {
        return $this->viewPositionId;
    }

    function setViewPositionId(string $viewPositionId): BaseAttachment {
        $this->viewPositionId = $viewPositionId;
        return $this;
    }

    /**
     * 
     * @return string
     */
    function getViewHtml() {
        return $this->viewHtml;
    }

    /**
     * 
     * @param callable $callback Parameter $this->viewHtml
     */
    public function replaceViewHtml(callable $callback): bool {
        $viewHtml = call_user_func($callback, $this->viewHtml);
        if ($viewHtml) {
            $this->viewHtml = $viewHtml;
            return true;
        }
        return false;
    }

}
